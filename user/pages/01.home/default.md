---
title: Home
visible: false
media_order: 'decibels.png,utc.png'
---

<div class="body-back"> </div>


<div class="accueil-intro">
    <p class="background intro">
    FSC (Flexion Social Club) est une association étudiante de l'Université Technologique de Compiègne, elle rassemble un grand nombre de passioné.es de musique. C'est environ une quarantaine de DJs qui apprennent à mixer de nombreux style de musique et performent dans différents formats de soirées/évènements que vous pouvez retrouver sur la page évènements. </p>
    <div class="accueil-back"></div>
</div>

<div class="partenaires background">

<bold>Nos partenaires</bold>

<div markdown="1" class="partenaires-list"> 
![utc](utc.png "utc")
       
![decibels](decibels.png "decibels")
</div>

</div>

<div class="footer">

</div>
