---
title: Contact
template: page
body_classes: gris
media_order: 'fsc.jpg,empty-profile.png'
---

# Contact

<div class="contact" markdown="1">
![fsc](fsc.jpg "fsc")
    <div markdown="1">
Président : Leo Peron

Mail : fsc@assos.utc.fr

Tel : +33 6 23 27 19 80

Adresse : 1 rue Roger Coutollenc, 60200, Compiègne
    </div>
</div>
