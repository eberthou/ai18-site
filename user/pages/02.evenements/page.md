---
title: Évènements
media_order: 'aftercomet.png,aftergoodchill.png,compilation.png,empty-profile.png,fsc.jpg,hippi.png,reggae.jpg,afterdeli.jpg,aftersport.jpg,disco.png,escom.jpg,galaescom.png'
template: page
body_classes: violet
---

# Évènements

# À l'UTC

> L’association s’occupe de la musique dans de nombreux évènements internes à l’UTC appelés Permanence.
> Les DJ suivent le thème de la soirée pour leurs mix.
> Des style musicaux différents sont joués pour plaire aux nombreuses demandes des UTCéens .

<div markdown="1" class="event">
- ![FSCompilation](compilation.png)
- ![Disco](disco.png)
- ![Perm Reggae](reggae.jpg)
</div>

# Autres écoles

> L’association s’occupe aussi de nombreux évènements pour d’autres écoles, notamment l’école ESCOM à Compiègne.

<div markdown="1" class="event">
- ![Escom](escom.jpg)
- ![GalaEscom](galaescom.png)
</div>

# Bars

> FSC est souvent recruté par les bars aux alentours pour apporter différents styles musicaux en fonction des évènements.
> Nous avons notamment travaillé avec le Goodchill et le Delirium à Compiègne.

<div markdown="1" class="event">
- ![AfterDeli](afterdeli.jpg)
- ![AfterGoodchill](aftergoodchill.png)
</div>

# Boites de nuits

> L’association s’occupe de mixer dans les boîtes de nuit des alentours de Compiègne.
> Cela permet d’assurer aux boîtes de nuit d’avoir une clientèle étudiantes venant pour écouter la musique de leurs DJ favoris. 

<div markdown="1" class="event">
- ![HippiEstu](hippi.png)
- ![AfterComet](aftercomet.png)
- ![AfterSport](aftersport.jpg)
</div>
