---
title: Musiques
template: page
body_classes: vert
media_order: 'disco.mp4,edm.mp4,france.jpg,hardstyle.jpg,hiphop.jpg,jungle.jpg,shatta.mp4,dub.jpg,france.mp4,hardstyle.mp4,hiphop.mp4,jungle.mp4,techno.jpg,dub.mp4,hardcore.jpg,hardtechno.jpg,house.jpg,techno.mp4,disco.jpg,edm.jpg,hardcore.mp4,hardtechno.mp4,house.mp4,shatta.jpg'
---

# Styles de musique

## 70 - 100 BPM

<div class="musiques">
    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/dub.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/dub.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/hiphop.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/hiphop.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/jungle.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/jungle.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/shatta.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/shatta.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

</div>


## 110 - 130 BPM

<div class="musiques">
    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/disco.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/disco.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/house.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/house.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/edm.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/edm.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
</div>

## 120 - 140 BPM

<div class="musiques">
    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/france.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/france.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/techno.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/techno.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
</div>

## 150++ BPM

<div class="musiques">
    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/hardcore.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/hardcore.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/hardstyle.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/hardstyle.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>

    <div class="flip-card">
        <div class="flip-card-inner">
            <div class="flip-card-front">
                <img src="/user/pages/04.musiques/hardtechno.jpg" style="width:400px;height:400px;">
            </div>
            <div class="flip-card-back">
                <video width="400" height="400">
                    <source src="/user/pages/04.musiques/hardtechno.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
</div>
