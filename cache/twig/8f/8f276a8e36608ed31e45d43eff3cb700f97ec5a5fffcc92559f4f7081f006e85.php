<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/base.html.twig */
class __TwigTemplate_2259968e691ef7b398c491344bce46b9ca864981a987a86d9b66ef2db494b03b extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
            'assets' => [$this, 'block_assets'],
            'header' => [$this, 'block_header'],
            'header_navigation' => [$this, 'block_header_navigation'],
            'body' => [$this, 'block_body'],
            'content' => [$this, 'block_content'],
            'bottom' => [$this, 'block_bottom'],
        ];
        $this->deferred = $this->env->getExtension('Twig\DeferredExtension\DeferredExtension');
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", []), "getActive", [])) ? ($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "language", []), "getActive", [])) : ($this->getAttribute($this->getAttribute($this->getAttribute(($context["grav"] ?? null), "config", []), "site", []), "default_lang", []))));
        echo "\">
<head>
";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 15
        echo "
";
        // line 16
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('javascripts', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('assets', $context, $blocks);
        // line 31
        echo "</head>
<body id=\"top\" class=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "header", []), "body_classes", []));
        echo "\">

";
        // line 34
        $this->displayBlock('header', $context, $blocks);
        // line 59
        echo "
";
        // line 60
        $this->displayBlock('body', $context, $blocks);
        // line 67
        echo "
";
        // line 68
        $this->displayBlock('bottom', $context, $blocks);
        // line 71
        echo "
</body>
</html>
";
        $this->deferred->resolve($this, $context, $blocks);
    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        // line 5
        echo "    <meta charset=\"utf-8\" />
    <title>";
        // line 6
        if ($this->getAttribute(($context["header"] ?? null), "title", [])) {
            echo twig_escape_filter($this->env, $this->getAttribute(($context["header"] ?? null), "title", []));
            echo " | ";
        }
        echo twig_escape_filter($this->env, $this->getAttribute(($context["site"] ?? null), "title", []));
        echo "</title>

    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    ";
        // line 10
        $this->loadTemplate("partials/metadata.html.twig", "partials/base.html.twig", 10)->display($context);
        // line 11
        echo "
    <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->urlFunc("theme://images/logo.png"));
        echo "\" />
    <link rel=\"canonical\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute(($context["page"] ?? null), "canonical", [0 => true], "method"));
        echo "\" />
";
    }

    // line 16
    public function block_stylesheets($context, array $blocks = [])
    {
        // line 17
        echo "    ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", [0 => "https://unpkg.com/purecss@1.0.0/build/pure-min.css", 1 => 100], "method");
        // line 18
        echo "    ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", [0 => "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css", 1 => 99], "method");
        // line 19
        echo "    ";
        $this->getAttribute(($context["assets"] ?? null), "addCss", [0 => "theme://css/custom.css", 1 => 98], "method");
    }

    // line 22
    public function block_javascripts($context, array $blocks = [])
    {
        // line 23
        echo "    ";
        $this->getAttribute(($context["assets"] ?? null), "addJs", [0 => "jquery", 1 => 100], "method");
    }

    public function block_assets($context, array $blocks = [])
    {
        $this->deferred->defer($this, 'assets');
    }

    // line 26
    public function block_assets_deferred($context, array $blocks = [])
    {
        // line 27
        echo "    ";
        echo $this->getAttribute(($context["assets"] ?? null), "css", [], "method");
        echo "
    ";
        // line 28
        echo $this->getAttribute(($context["assets"] ?? null), "js", [], "method");
        echo "
    <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->urlFunc("theme://js/video.js"), "html", null, true);
        echo "\" defer></script>
";
        $this->deferred->resolve($this, $context, $blocks);
    }

    // line 34
    public function block_header($context, array $blocks = [])
    {
        // line 35
        echo "    <div class=\"header\">
        <div class=\"navbar\">
            <a class=\"logo left\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, ($context["home_url"] ?? null));
        echo "\">
                <img src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->urlFunc("theme://images/logo.png"));
        echo "\">
            </a>
            ";
        // line 40
        $this->displayBlock('header_navigation', $context, $blocks);
        // line 56
        echo "        </div>
    </div>
";
    }

    // line 40
    public function block_header_navigation($context, array $blocks = [])
    {
        // line 41
        echo "            <nav class=\"main-nav\">
                ";
        // line 42
        $this->loadTemplate("partials/navigation.html.twig", "partials/base.html.twig", 42)->display($context);
        // line 43
        echo "                <div>
                    <a href=\"https://www.youtube.com/channel/UCzGgRLz8DQoTTeEcxBEWYGw/videos\" target=\"_blank\"> 
                        <img src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->urlFunc("theme://images/youtube.png"));
        echo "\"> 
                    </a>
                    <a href=\"https://www.facebook.com/groups/spinutcformation/\" target=\"_blank\"> 
                        <img src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->urlFunc("theme://images/facebook.png"));
        echo "\"> 
                    </a>
                    <a href=\"https://www.instagram.com/flexionsocial/\" target=\"_blank\"> 
                        <img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Grav\Common\Twig\Extension\GravExtension')->urlFunc("theme://images/instagram.png"));
        echo "\"> 
                    </a>
                </div>
            </nav>
            ";
    }

    // line 60
    public function block_body($context, array $blocks = [])
    {
        // line 61
        echo "    <section id=\"body\">
        <div class=\"wrapper padding\">
        ";
        // line 63
        $this->displayBlock('content', $context, $blocks);
        // line 64
        echo "        </div>
    </section>
";
    }

    // line 63
    public function block_content($context, array $blocks = [])
    {
    }

    // line 68
    public function block_bottom($context, array $blocks = [])
    {
        // line 69
        echo "    ";
        echo $this->getAttribute(($context["assets"] ?? null), "js", [0 => "bottom"], "method");
        echo "
";
    }

    public function getTemplateName()
    {
        return "partials/base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 69,  248 => 68,  243 => 63,  237 => 64,  235 => 63,  231 => 61,  228 => 60,  219 => 51,  213 => 48,  207 => 45,  203 => 43,  201 => 42,  198 => 41,  195 => 40,  189 => 56,  187 => 40,  182 => 38,  178 => 37,  174 => 35,  171 => 34,  164 => 29,  160 => 28,  155 => 27,  152 => 26,  142 => 23,  139 => 22,  134 => 19,  131 => 18,  128 => 17,  125 => 16,  119 => 13,  115 => 12,  112 => 11,  110 => 10,  99 => 6,  96 => 5,  93 => 4,  85 => 71,  83 => 68,  80 => 67,  78 => 60,  75 => 59,  73 => 34,  68 => 32,  65 => 31,  63 => 26,  60 => 25,  58 => 22,  55 => 21,  53 => 16,  50 => 15,  48 => 4,  43 => 2,  40 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"{{ (grav.language.getActive ?: grav.config.site.default_lang)|e }}\">
<head>
{% block head %}
    <meta charset=\"utf-8\" />
    <title>{% if header.title %}{{ header.title|e }} | {% endif %}{{ site.title|e }}</title>

    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    {% include 'partials/metadata.html.twig' %}

    <link rel=\"icon\" type=\"image/png\" href=\"{{ url('theme://images/logo.png')|e }}\" />
    <link rel=\"canonical\" href=\"{{ page.canonical(true)|e }}\" />
{% endblock head %}

{% block stylesheets %}
    {% do assets.addCss('https://unpkg.com/purecss@1.0.0/build/pure-min.css', 100) %}
    {% do assets.addCss('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', 99) %}
    {% do assets.addCss('theme://css/custom.css', 98) %}
{% endblock %}

{% block javascripts %}
    {% do assets.addJs('jquery', 100) %}
{% endblock %}

{% block assets deferred %}
    {{ assets.css()|raw }}
    {{ assets.js()|raw }}
    <script src=\"{{url('theme://js/video.js')}}\" defer></script>
{% endblock %}
</head>
<body id=\"top\" class=\"{{ page.header.body_classes|e }}\">

{% block header %}
    <div class=\"header\">
        <div class=\"navbar\">
            <a class=\"logo left\" href=\"{{ home_url|e }}\">
                <img src=\"{{ url('theme://images/logo.png')|e }}\">
            </a>
            {% block header_navigation %}
            <nav class=\"main-nav\">
                {% include 'partials/navigation.html.twig' %}
                <div>
                    <a href=\"https://www.youtube.com/channel/UCzGgRLz8DQoTTeEcxBEWYGw/videos\" target=\"_blank\"> 
                        <img src=\"{{ url('theme://images/youtube.png')|e }}\"> 
                    </a>
                    <a href=\"https://www.facebook.com/groups/spinutcformation/\" target=\"_blank\"> 
                        <img src=\"{{ url('theme://images/facebook.png')|e }}\"> 
                    </a>
                    <a href=\"https://www.instagram.com/flexionsocial/\" target=\"_blank\"> 
                        <img src=\"{{ url('theme://images/instagram.png')|e }}\"> 
                    </a>
                </div>
            </nav>
            {% endblock %}
        </div>
    </div>
{% endblock %}

{% block body %}
    <section id=\"body\">
        <div class=\"wrapper padding\">
        {% block content %}{% endblock %}
        </div>
    </section>
{% endblock %}

{% block bottom %}
    {{ assets.js('bottom')|raw }}
{% endblock %}

</body>
</html>
", "partials/base.html.twig", "/var/www/html/user/themes/ai18/templates/partials/base.html.twig");
    }
    private $deferred;
}
