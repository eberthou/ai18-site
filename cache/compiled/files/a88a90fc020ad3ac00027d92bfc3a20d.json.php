<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledJsonFile',
    'filename' => '/var/www/html/user/data/flex/indexes/pages.json',
    'modified' => 1718785453,
    'size' => 1469,
    'data' => [
        'version' => '1.5',
        'timestamp' => 1718785453,
        'count' => 7,
        'index' => [
            '' => [
                'key' => '',
                'storage_key' => '',
                'template' => NULL,
                'storage_timestamp' => 1718717731,
                'children' => [
                    '01.home' => 1718493198,
                    '02.evenements' => 1718717479,
                    '03.photos' => 1717576830,
                    '04.demo' => 1717577289,
                    '04.musiques' => 1718717731,
                    '05.contact' => 1718717443
                ],
                'checksum' => 'ed76831c17d5feb80845affa5f2d09a4'
            ],
            '01.home' => [
                'key' => 'home',
                'storage_key' => '01.home',
                'template' => 'default',
                'storage_timestamp' => 1718785453,
                'markdown' => [
                    '' => [
                        'default' => 1718785453
                    ]
                ],
                'checksum' => '02db45946f56b0267275d0a2cf4256b1'
            ],
            '02.evenements' => [
                'key' => 'evenements',
                'storage_key' => '02.evenements',
                'template' => 'page',
                'storage_timestamp' => 1718724061,
                'markdown' => [
                    '' => [
                        'page' => 1718724061
                    ]
                ],
                'checksum' => '7ba9630c972754d95b201c02e8738ee4'
            ],
            '03.photos' => [
                'key' => 'photos',
                'storage_key' => '03.photos',
                'template' => 'page',
                'storage_timestamp' => 1718784915,
                'markdown' => [
                    '' => [
                        'page' => 1718784915
                    ]
                ],
                'checksum' => 'ef4c153246f580f1e8280ff8479a8ae6'
            ],
            '04.musiques' => [
                'key' => 'musiques',
                'storage_key' => '04.musiques',
                'template' => 'page',
                'storage_timestamp' => 1718723018,
                'markdown' => [
                    '' => [
                        'page' => 1718722788
                    ]
                ],
                'checksum' => 'f11410582bb11766d3e141997dbd32da'
            ],
            '05.demo' => [
                'key' => 'demo',
                'storage_key' => '05.demo',
                'template' => 'page',
                'storage_timestamp' => 1718721390,
                'markdown' => [
                    '' => [
                        'page' => 1718721390
                    ]
                ],
                'checksum' => '87ccfd11f128b51c5bb03a7934ac3a82'
            ],
            '06.contact' => [
                'key' => 'contact',
                'storage_key' => '06.contact',
                'template' => 'page',
                'storage_timestamp' => 1718722234,
                'markdown' => [
                    '' => [
                        'page' => 1718722234
                    ]
                ],
                'checksum' => 'a4c9d2cfa02e4ed453778b009f00595f'
            ]
        ]
    ]
];
