<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/html/user/pages/04.demo/page.md',
    'modified' => 1717577289,
    'size' => 1762,
    'data' => [
        'header' => [
            'title' => 'Démonstration',
            'template' => 'page',
            'body_classes' => 'bleu',
            'media_order' => 'fsc.jpg,empty-profile.png'
        ],
        'frontmatter' => 'title: Démonstration
template: page
body_classes: bleu
media_order: \'fsc.jpg,empty-profile.png\'',
        'markdown' => '# Démonstration

## Vidéos

<iframe width="560" height="315" src="https://www.youtube.com/embed/hNIhJKRRstk?si=xuQfSMvT4bxAnPNq" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/-WhPLd5XuRI?si=P5RM0doErEnB_wRJ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## Soundcloud

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1655961954&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/flexion-social-club" title="Flexion Social Club" target="_blank" style="color: #cccccc; text-decoration: none;">Flexion Social Club</a> · <a href="https://soundcloud.com/flexion-social-club/rekord-room-01-mrcr" title="Rekord Room 01 - MRCR" target="_blank" style="color: #cccccc; text-decoration: none;">Rekord Room 01 - MRCR</a></div>'
    ]
];
