<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/plugins/devtools/languages.yaml',
    'modified' => 1716633652,
    'size' => 165,
    'data' => [
        'en' => [
            'PLUGIN_DEVTOOLS' => [
                'COLLISION_CHECK' => 'Online Name Collision Check'
            ]
        ],
        'fr' => [
            'PLUGIN_DEVTOOLS' => [
                'COLLISION_CHECK' => 'Vérification en ligne de collision de noms'
            ]
        ]
    ]
];
