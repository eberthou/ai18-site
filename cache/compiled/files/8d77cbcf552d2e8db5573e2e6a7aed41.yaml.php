<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://devtools/devtools.yaml',
    'modified' => 1716633652,
    'size' => 36,
    'data' => [
        'enabled' => true,
        'collision_check' => true
    ]
];
