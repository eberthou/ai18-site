<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/themes/ai18/blueprints.yaml',
    'modified' => 1716633720,
    'size' => 756,
    'data' => [
        'name' => 'Ai18',
        'slug' => 'ai18',
        'type' => 'theme',
        'version' => '0.1.0',
        'description' => 'Theme pour AI18',
        'icon' => 'rebel',
        'author' => [
            'name' => 'Evann BERTHOU',
            'email' => 'evann.berthou@gmail.com'
        ],
        'homepage' => 'https://github.com/evann-berthou/grav-theme-ai18',
        'demo' => 'http://demo.yoursite.com',
        'keywords' => 'grav, theme, etc',
        'bugs' => 'https://github.com/evann-berthou/grav-theme-ai18/issues',
        'readme' => 'https://github.com/evann-berthou/grav-theme-ai18/blob/develop/README.md',
        'license' => 'MIT',
        'dependencies' => [
            0 => [
                'name' => 'grav',
                'version' => '>=1.6.0'
            ]
        ],
        'form' => [
            'validation' => 'loose',
            'fields' => [
                'dropdown.enabled' => [
                    'type' => 'toggle',
                    'label' => 'Dropdown in Menu',
                    'highlight' => 1,
                    'default' => 1,
                    'options' => [
                        1 => 'PLUGIN_ADMIN.ENABLED',
                        0 => 'PLUGIN_ADMIN.DISABLED'
                    ],
                    'validate' => [
                        'type' => 'bool'
                    ]
                ]
            ]
        ]
    ]
];
