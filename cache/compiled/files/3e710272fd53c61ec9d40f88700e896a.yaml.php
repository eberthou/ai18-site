<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/accounts/admin.yaml',
    'modified' => 1716212189,
    'size' => 218,
    'data' => [
        'state' => 'enabled',
        'email' => 'admin@admin.com',
        'fullname' => 'admin',
        'title' => 'admin',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$2apZX.fAXlfO0W8AhmMQPuVcQV2Pp9J/37rvlXGfepmvfNhZoCz6W'
    ]
];
