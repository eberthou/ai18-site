<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/html/user/pages/04.contact/page.md',
    'modified' => 1717576614,
    'size' => 1227,
    'data' => [
        'header' => [
            'title' => 'Contact',
            'template' => 'page',
            'body_classes' => 'vert',
            'media_order' => 'fsc.jpg,empty-profile.png'
        ],
        'frontmatter' => 'title: Contact
template: page
body_classes: vert
media_order: \'fsc.jpg,empty-profile.png\'',
        'markdown' => '# Contact

<div class="contact" markdown="1">
![fsc](fsc.jpg "fsc")
    <div markdown="1">
Mail: fsc@assos.utc.fr

Tel: +33 6 11 11 11 11

Adresse : 1 rue Roger Coutollenc, 60200, Compiègne
    </div>
</div>


<div class="membres">
    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>

    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>
    
    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>
    
    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>
    
    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>
    
    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>
    
    <div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>

	<div class="membre" markdown="1">
![leo](empty-profile.png "leo")

### Leo Peron

Président
    </div>
</div>
'
    ]
];
