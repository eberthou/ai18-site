<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/html/user/pages/03.photos/default.md',
    'modified' => 1716648114,
    'size' => 582,
    'data' => [
        'header' => [
            'title' => 'Photos',
            'media_order' => 'photo1.jpg,photo2.jpg,photo3.jpg,photo4.jpg,photo5.jpg',
            'template' => 'page',
            'body_classes' => 'orange'
        ],
        'frontmatter' => 'title: Photos
media_order: \'photo1.jpg,photo2.jpg,photo3.jpg,photo4.jpg,photo5.jpg\'
template: page
body_classes: orange',
        'markdown' => '# Photos

## OpenFlex

<div markdown="1" class="photos">
- ![photo1](photo1.jpg "photo1")
- ![photo2](photo2.jpg "photo1")
- ![photo3](photo3.jpg "photo1")
- ![photo4](photo4.jpg "photo1")
- ![photo5](photo5.jpg "photo1")
</div>

## Autre soirée

<div markdown="1" class="photos">
- ![photo1](photo1.jpg "photo1")
- ![photo2](photo2.jpg "photo1")
- ![photo3](photo3.jpg "photo1")
- ![photo4](photo4.jpg "photo1")
- ![photo5](photo5.jpg "photo1")
</div>'
    ]
];
