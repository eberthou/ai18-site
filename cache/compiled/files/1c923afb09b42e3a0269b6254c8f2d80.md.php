<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/html/user/pages/02.evenements/default.md',
    'modified' => 1716649067,
    'size' => 511,
    'data' => [
        'header' => [
            'title' => 'Évènements',
            'media_order' => 'photo4.png,photo3.png,photo2.png,photo1.png',
            'template' => 'page',
            'body_classes' => 'violet'
        ],
        'frontmatter' => 'title: Évènements
media_order: \'photo4.png,photo3.png,photo2.png,photo1.png\'
template: page
body_classes: violet',
        'markdown' => '# Photos

## OpenFlex

<div markdown="1" class="photos">
- ![photo1](photo1.png "photo1")
- ![photo2](photo2.png "photo1")
- ![photo3](photo3.png "photo1")
- ![photo4](photo4.png "photo1")
</div>

## Autre soirée

<div markdown="1" class="photos">
- ![photo1](photo1.png "photo1")
- ![photo2](photo2.png "photo1")
- ![photo3](photo3.png "photo1")
- ![photo4](photo4.png "photo1")
</div>'
    ]
];
