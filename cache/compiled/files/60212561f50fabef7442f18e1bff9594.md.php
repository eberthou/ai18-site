<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledMarkdownFile',
    'filename' => '/var/www/html/user/pages/06.contact/page.md',
    'modified' => 1718722234,
    'size' => 334,
    'data' => [
        'header' => [
            'title' => 'Contact',
            'template' => 'page',
            'body_classes' => 'gris',
            'media_order' => 'fsc.jpg,empty-profile.png'
        ],
        'frontmatter' => 'title: Contact
template: page
body_classes: gris
media_order: \'fsc.jpg,empty-profile.png\'',
        'markdown' => '# Contact

<div class="contact" markdown="1">
![fsc](fsc.jpg "fsc")
    <div markdown="1">
Président : Leo Peron

Mail : fsc@assos.utc.fr

Tel : +33 6 23 27 19 80

Adresse : 1 rue Roger Coutollenc, 60200, Compiègne
    </div>
</div>
'
    ]
];
